package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class UpdateTempStatusCredRequestDTO implements Serializable {

    private Long idCredTemp;
    private Long idEstaCred;
    private String userReq;
    private String serial;

}