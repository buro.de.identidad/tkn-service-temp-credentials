package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CredentialsValidationFingersRequestDTO implements Serializable {

    private String username;
    private String password;
    private String serial;
    private String userRequesting;

}