package com.teknei.bid.dto;

import lombok.Data;

import java.io.Serializable;

@Data
public class CredentialsDeleteRequestDTO implements Serializable {

    private String username;
    private String usernameRequesting;
    private Integer noFingers;
    private String serial;


}