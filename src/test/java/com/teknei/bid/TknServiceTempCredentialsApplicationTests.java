package com.teknei.bid;

import com.teknei.bid.controller.rest.CredentialsTempController;
import com.teknei.bid.dto.*;
import com.teknei.bid.persistence.entities.CbidCredTemp;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.junit.Assert.assertEquals;

//@RunWith(SpringRunner.class)
//@SpringBootTest
public class TknServiceTempCredentialsApplicationTests {

    //@Autowired
    private CredentialsTempController credentialsTempController;

    private static final Logger log = LoggerFactory.getLogger(TknServiceTempCredentialsApplicationTests.class);

    //@Test
    public void testRetrieve(){
        //ResponseEntity<List<CbidCredTemp>> listResponseEntity = credentialsTempController.findAll();
        //log.info("List entity response: {}", listResponseEntity);
        //assertEquals(listResponseEntity.getStatusCodeValue(), 200);
    }

    //@Test
    public void testCreation() {
        CredentialsSaveRequestDTO saveRequestDTO = new CredentialsSaveRequestDTO();
        saveRequestDTO.setName("JORGE");
        saveRequestDTO.setNoFingers(10);
        saveRequestDTO.setPassword("ABC");
        saveRequestDTO.setSerial("16062510177");
        saveRequestDTO.setSurnameFirst("AMARO");
        saveRequestDTO.setSurnameLast("CORIA");
        saveRequestDTO.setUserRequesting("bid");
        saveRequestDTO.setUsername("jamaro123005");

        ResponseEntity<CredentialsValidationResultDTO> responseEntity = credentialsTempController.create(saveRequestDTO);
        assertEquals(200, responseEntity.getStatusCodeValue());

        CredentialsValidationRequestDTO validationRequestDTO = new CredentialsValidationRequestDTO();
        validationRequestDTO.setPassword("ABC");
        validationRequestDTO.setSerial("16062510177");
        validationRequestDTO.setUsername("jamaro123005");
        validationRequestDTO.setUserRequesting("bid");

        ResponseEntity<CredentialsValidationResultDTO> responseEntity1 = credentialsTempController.validate(validationRequestDTO);
        assertEquals(200, responseEntity1.getStatusCodeValue());

        CredentialsDeleteRequestDTO deleteRequestDTO = new CredentialsDeleteRequestDTO();
        deleteRequestDTO.setNoFingers(10);
        deleteRequestDTO.setNoFingers(10);
        deleteRequestDTO.setSerial("16062510177");
        deleteRequestDTO.setUsername("jamaro123005");
        deleteRequestDTO.setUsernameRequesting("bid");

        ResponseEntity<CredentialsValidationResultDTO> responseEntity2 = credentialsTempController.delete(deleteRequestDTO);
        assertEquals(422, responseEntity2.getStatusCodeValue());

        CredentialsValidationFingersRequestDTO validationFingersRequestDTO = new CredentialsValidationFingersRequestDTO();
        validationFingersRequestDTO.setPassword("ABC");
        validationFingersRequestDTO.setSerial("16062510177");
        validationFingersRequestDTO.setUsername("jamaro123005");
        validationFingersRequestDTO.setUserRequesting("bid");

        ResponseEntity<CredentialsValidationResultDTO> responseEntity3 = credentialsTempController.validateNoFingers(validationFingersRequestDTO);
        assertEquals(200, responseEntity3.getStatusCodeValue());
        assertEquals(10, responseEntity3.getBody().getStatus().intValue());

    }

}
